import { createApp } from 'vue'
import App from './App.vue'


// Es lo primero que arranca . Si deseas generar variables globales
createApp(App).mount('#app')
